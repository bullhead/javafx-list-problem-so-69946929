package com.stackoverflow.security

import com.alercode.security.Message
import javafx.geometry.Pos
import javafx.scene.control.ContentDisplay
import javafx.scene.paint.Color
import tornadofx.*
import java.text.SimpleDateFormat
import java.util.*

class OutgoingMessageView(onTap: (Message) -> Unit) : MessageView(onTap) {
    private var message: Message? = null

    private val formatter = SimpleDateFormat("dd-MMM-yyyy")
    private val textLabel = label {
        maxWidth = 550.0
        addClass(Style.textLabel)
        isWrapText = true
    }

    private val captionLabel = label {
        addClass(Style.captionLabel)
        contentDisplay = ContentDisplay.RIGHT
    }

    override val root = hbox {
        setOnMouseClicked {
            message?.let(onTap)
        }
        alignment = Pos.CENTER_RIGHT
        style {
            padding = box(8.px, 8.px, 8.px, 30.px)
        }
        vbox {
            alignment = Pos.CENTER_LEFT
            minWidth = 50.0
            importStylesheet(Style::class)
            addClass(Style.outgoingRoot)
            add(textLabel)
            hbox {
                alignment = Pos.CENTER_RIGHT
                add(captionLabel)
            }
        }
    }


    override fun bind(message: Message, selected: Boolean) {
        this.message = message
        if (message.text.isNotEmpty()) {
            textLabel.show()
            textLabel.text = message.text
        } else {
            textLabel.hide()
        }
        captionLabel.text = formatter.format(Date(message.time))
        val c = if (selected) Color.ROSYBROWN else Color.TRANSPARENT
        root.style(true) {
            backgroundColor += c
        }
    }


    class Style : Stylesheet() {
        companion object {
            val textLabel by cssclass()
            val outgoingRoot by cssclass()
            val captionLabel by cssclass()
        }

        init {
            outgoingRoot {
                padding = box(5.px, 10.px, 5.px, 10.px)
                backgroundColor += c("#4caf50")
                backgroundRadius += box(10.px, 10.px, 0.px, 10.px)
            }
            textLabel {
                fontSize = 14.px
                textFill = Color.WHITE
            }
            captionLabel {
                fontSize = 10.px
                textFill = Color.WHITESMOKE
            }
        }
    }
}
