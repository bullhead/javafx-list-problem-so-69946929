package com.stackoverflow.security

import javafx.scene.paint.Color
import tornadofx.*

class Theme : Stylesheet() {

    init {
        root {
            backgroundColor += c("#FAFAFA")
            fontSize = 12.px
            accentColor = c("#4caf50")
        }
        listView {
            backgroundInsets += box(0.px)
            listCell {
                // cellSize=300.px
                and(even) {
                    and(selected) {
                        backgroundColor += c("#4caf50").derive(0.9)
                    }
                    backgroundColor += Color.TRANSPARENT
                }
                and(odd) {
                    and(selected) {
                        backgroundColor += c("#4caf50").derive(0.9)
                    }
                    backgroundColor += Color.TRANSPARENT
                }
            }
        }
    }
}