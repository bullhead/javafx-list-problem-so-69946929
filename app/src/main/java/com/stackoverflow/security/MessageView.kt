package com.stackoverflow.security

import com.alercode.security.Message
import tornadofx.View

abstract class MessageView(val onTap: (Message) -> Unit) : View() {
    abstract fun bind(message: Message, selected: Boolean)
}