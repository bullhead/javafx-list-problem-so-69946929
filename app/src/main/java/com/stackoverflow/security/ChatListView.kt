package com.stackoverflow.security

import com.alercode.security.Message
import com.alercode.security.MessagesProvider
import io.reactivex.rxjava3.core.Observable
import io.reactivex.rxjava3.disposables.Disposable
import io.reactivex.rxjava3.schedulers.Schedulers
import javafx.collections.FXCollections
import javafx.scene.layout.Background
import javafx.scene.layout.HBox
import javafx.scene.layout.Priority
import javafx.scene.paint.Color
import org.fxmisc.flowless.Cell
import org.fxmisc.flowless.VirtualFlow
import org.fxmisc.flowless.VirtualizedScrollPane
import org.pdfsam.rxjavafx.schedulers.JavaFxScheduler
import org.slf4j.LoggerFactory
import tornadofx.View
import tornadofx.hbox
import tornadofx.hgrow
import tornadofx.style
import java.util.*
import java.util.concurrent.TimeUnit


@Suppress("JAVA_MODULE_DOES_NOT_DEPEND_ON_MODULE")
class ChatListView : View() {
    private val logger = LoggerFactory.getLogger(ChatListView::class.java)
    private val dataProvider: MessagesProvider by di()
    private val data = FXCollections.observableArrayList<Message>()
    private var disposable: Disposable? = null
    private var scrollScheduler: Disposable? = null
    private var selected: Message? = null
    private var cell: Cell<Message, HBox>? = null
    private val virtualFlow = VirtualFlow.createVertical(data) {
        Cell.wrapNode(showMessage(it))
    }
    private val pane = VirtualizedScrollPane(virtualFlow).apply {
        hgrow = Priority.ALWAYS
        stylesheets.add(resources.url("/scrollpane.css").toExternalForm())
    }
    override val root = hbox {
        prefHeight = 800.0
        prefWidth = 1300.0
        add(pane)
    }


    init {
        disposable = dataProvider.messages()
            .subscribeOn(Schedulers.io())
            .observeOn(JavaFxScheduler.platform())
            .subscribe({
                logger.info("Loaded all messages for conversation")
                data.addAll(it)
                scrollSafely()
                sc()
            }, {
                logger.info("failed to load all messages for conversation")
            })

    }

    private fun showMessage(message: Message): HBox {
        return if (message.self) {
            val v = OutgoingMessageView {
                onTap(it)
            }
            v.bind(message, message.id == selected?.id)
            v.root
        } else {
            val v = IncomingMessageView {
                onTap(it)
            }
            v.bind(message, message.id == selected?.id)
            v.root
        }
    }

    private fun onTap(message: Message) {
        var previousIndex = -1
        selected = if (this.selected != null && this.selected!!.id == message.id) {
            null
        } else {
            previousIndex = if (selected == null) -1 else data.indexOfFirst { it.id == selected!!.id }
            message
        }

        if (previousIndex > -1) {
            val cellOpt = virtualFlow.getCellIfVisible(previousIndex)
            if (cellOpt.isPresent) {
                val cell = cellOpt.get()
                logger.info("Updated previous position")
                cell.node.background = Background.EMPTY
            }
        }
        val index = data.indexOfFirst { it.id == message.id }
        val v = virtualFlow.getCellIfVisible(index)
        if (v.isPresent) {
            val x = v.get()
            x.node.style(true) {
                backgroundColor += if (selected != null) Color.ROSYBROWN else Color.TRANSPARENT
            }
        }

    }

    private fun sc() {
        var self = true
        Observable.timer(5, TimeUnit.SECONDS)
            .repeat()
            .observeOn(JavaFxScheduler.platform())
            .subscribe {
                data.add(
                    Message(
                        UUID.randomUUID().toString(),
                        "[JavaFX Application Thread] INFO com.stackoverflow.security.ChatListView - Loaded all messages for conversation",
                        self,
                        System.currentTimeMillis()
                    )
                )
                self = self.not()
                //scrollSafely()
            }
    }


    private fun scrollSafely() {
        scrollScheduler?.dispose()
        scrollScheduler = Observable.timer(200, TimeUnit.MILLISECONDS)
            .observeOn(JavaFxScheduler.platform())
            .subscribe {
                virtualFlow.show(data.size - 1)
            }
    }

}