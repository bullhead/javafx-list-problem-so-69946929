package com.stackoverflow.security


import com.google.inject.Guice
import com.google.inject.Injector
import javafx.scene.Scene
import javafx.scene.paint.Color
import javafx.stage.Stage
import tornadofx.App
import tornadofx.DIContainer
import tornadofx.FX
import tornadofx.UIComponent
import java.util.*
import kotlin.reflect.KClass
import kotlin.system.exitProcess

class App : App(ChatListView::class, Theme::class) {
    val guice: Injector = Guice.createInjector()

    init {
        FX.locale = Locale.forLanguageTag("tr")
        FX.dicontainer = object : DIContainer {
            override fun <T : Any> getInstance(type: KClass<T>) = guice.getInstance(type.java)
        }
    }

    override fun createPrimaryScene(view: UIComponent): Scene {
        val scene = super.createPrimaryScene(view)
        scene.fill = Color.TRANSPARENT
        return scene
    }

    override fun start(stage: Stage) {
        stage.setOnCloseRequest {
            exitProcess(0)
        }
        super.start(stage)
    }

}