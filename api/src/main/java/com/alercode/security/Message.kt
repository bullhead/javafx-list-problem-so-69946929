package com.alercode.security

data class Message(val id: String, val text: String, val self: Boolean, val time: Long)
