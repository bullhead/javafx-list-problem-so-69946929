package com.alercode.security

import com.google.inject.Inject
import com.google.inject.Singleton
import io.reactivex.rxjava3.core.Single
import java.util.*

@Singleton
class MessagesProvider @Inject constructor() {
    companion object {
        const val m = """
            The Kotlin Gradle plugin was loaded multiple times in different subprojects, which is not supported and may break the build. 
This might happen in subprojects that apply the Kotlin plugins with the Gradle 'plugins { ... }' DSL if they specify explicit versions, even if the versions are equal.
Please add the Kotlin plugin to the common parent project or the root project, then remove the versions in the subprojects.
If the parent project does not need the plugin, add 'apply false' to the plugin line.
See: https://docs.gradle.org/current/userguide/plugins.html#sec:subprojects_plugins_dsl
The Kotlin plugin was loaded in the following projects: ':api', ':app'
        """
        const val l = "Short message"
    }

    fun messages(): Single<List<Message>> {
        return Single.create {
            val list = mutableListOf<Message>()
            for (i in 1..2000) {
                list.add(
                    Message(
                        UUID.randomUUID().toString(),
                        if (i.mod(3) == 0) l else m,
                        i.mod(2) == 0,
                        System.currentTimeMillis()
                    )
                )
            }
            if (!it.isDisposed) {
                it.onSuccess(list)
            }
        }
    }
}